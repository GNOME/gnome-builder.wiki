# Develop applications for GNOME

GNOME Builder is an IDE focused on creating applications for the [GNOME desktop](https://gnome.org).

## Installing Builder

The easiest way to install GNOME Builder is to use Flatpak on your Linux-based workstation or laptop. See [Builder on Flathub](https://flathub.org/apps/details/org.gnome.Builder) for details.

## Documentation

Currently, our documentation can be found at [builder.readthedocs.io](https://builder.readthedocs.io).

## Contributing

Check our our issue tracker to find issues with the `newcomer` tag.

We can also be found on IRC at `irc://irg.gnome.org/#gnome-builder` for real-time communication.

## Getting Help

Visit GNOME's [workflow for Newcomers](https://wiki.gnome.org/Newcomers) to get an idea on how to contribute to GNOME at large. Builder follows typical GNOME best practices.

You can also file an issue on our issue tracker and we'll do what we can to help you.