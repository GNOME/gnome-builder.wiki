The following groups are ideas for new features if people would like to work on them. It would be wise to create a specification for how the feature should work as that will likely be a requirement for anyone to actually implement your requested feature.

# Editor

## Code Foundry Integration

 * Add menu item for finding references within same file only
 * Pre-select or otherwise highlight reference when navigating Find References

## Keybindings

 *  Kakoune/Helix keyboard theme

# Code Foundry

## Flatpak

 * Elevate errors parsing flatpak manifest to the user (will require some way to be relatively certain it's actually a Flatpak manifest first to not spam users with similarly-but-different formatted JSON documents).
 * Support for YAML Flatpak manifests
 * Add support for icon-themes when running applications
